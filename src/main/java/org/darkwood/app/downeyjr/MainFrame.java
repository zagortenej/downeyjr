package org.darkwood.app.downeyjr;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;


public class MainFrame extends JFrame
{
    private static final long serialVersionUID = -5000268324684378347L;
    
    private int mouseClickX;
    private int mouseClickY;
    
    String[] backgroundImageLocations = {
            "/backgrounds/01.jpg",
            "/backgrounds/02.jpg",
            "/backgrounds/03.jpg",
            "/backgrounds/04.jpg",
            "/backgrounds/05.jpg",
            "/backgrounds/06.jpg",
            "/backgrounds/07.jpg",
            "/backgrounds/08.jpg",
            "/backgrounds/09.jpg",
            "/backgrounds/10.jpg",
            "/backgrounds/11.jpg",
            "/backgrounds/12.jpg",
            "/backgrounds/13.jpg",
            "/backgrounds/14.jpg",
            "/backgrounds/15.jpg",
            "/backgrounds/16.jpg",
            "/backgrounds/17.jpg",
            "/backgrounds/18.jpg",
            "/backgrounds/19.jpg",
            "/backgrounds/20.jpg",
            "/backgrounds/saint-genesis-gally-2.jpg",
    };

    // milliseconds
    final int backgroundSwitchDelay = 30000;
    int nextBackgroundIndex = 0;

    List<ImageIcon> backgroundImages = new ArrayList<>();

    private JLabel background;
    
    static SplashWindow splash = null;

    public MainFrame() throws IOException {
        // get rid of default system provided window decorations
        setUndecorated(true);
        // make root pane provide window decorations
//        getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        URL appIconLocation = getClass().getResource( "/images/appIcon.png" );
        ImageIcon appIcon = new ImageIcon( appIconLocation );
        setIconImage( appIcon.getImage() );

        preloadAllBackgroundImages();

        background = new JLabel();
        setNextBackgroundImage();

        createButtons(background);

        // this is default, so it's just to be verbose
        setLayout(new BorderLayout());
        add(background, BorderLayout.CENTER);

        createWindowDraggingHandlers();
        
        // javax.swing timer to switch background images
        new Timer(backgroundSwitchDelay, (event) -> {
            setNextBackgroundImage();
        }).start();
        
        addWindowFocusListener(new WindowFocusListener() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                // keep window on the bottom of the window stack
                // that's its purpose after all, to behave as a sort of background image, eh?
//                toBack();
            }

            @Override
            public void windowLostFocus(WindowEvent e) {
                // don't care
            }
        });

        // two standard ways to handle main window closing and app exiting
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // or
//        addWindowListener( new WindowAdapter()
//        {
//            @Override
//            public void windowClosing( WindowEvent e )
//            {
//                // Do some other things before main window closes and app exits
//                System.out.println("Closing main window");
//                dispose();
//            }
//        });

        // calculate layout based on preferred component sizes
        // this sets preferred main frame size
//        pack();
        // set our own size
        setSize( 800, 600 );

        // centers the main frame.
        // it needs to know frame size so it should be called after pack() and/or setSize()
        setLocationRelativeTo(null);

        setVisible( true );
    }

    private void preloadAllBackgroundImages() throws IOException {
        splash.setNumberOfImages(backgroundImageLocations.length);
        for(String imageLocation: backgroundImageLocations) {
            final URL resourceStream = this.getClass().getResource(imageLocation);
            final BufferedImage bufferedImage = ImageIO.read(resourceStream);
            final ImageIcon imageIcon = new ImageIcon(bufferedImage);
            backgroundImages.add(imageIcon);
            splash.progressStep();
        }
        splash.close();
    }

    private void createWindowDraggingHandlers() {
        // these two listeners are to implement window dragging
        // by click-and-drag on background image.
        // keeps track of where mouse was when user clicked on background
        background.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                mouseClickX = e.getX();
                mouseClickY = e.getY();
            }
        });
        // moves main frame based on how far mouse is dragged from where user initially clicked
        background.addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent evt) {
                Rectangle rectangle = getBounds();
                setBounds(evt.getXOnScreen() - mouseClickX, evt.getYOnScreen() - mouseClickY, rectangle.width, rectangle.height);
            }
        });
    }

    private void setNextBackgroundImage() {
        if(nextBackgroundIndex > backgroundImages.size()) {
            nextBackgroundIndex = 0;
        }
        background.setIcon(backgroundImages.get(nextBackgroundIndex));
        nextBackgroundIndex++;
    }

    private void createButtons(JLabel background) throws IOException {
        // close the window and end the app
        BufferedImage cross = ImageIO.read(this.getClass().getResource("/images/cross.png"));
        JButton btnCloseMainwindow = createWindowControlButton(
                new ImageIcon(cross),
                (event) -> {
                    // nice way to close the main window and thus end the application
                    dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
                });

        // minimize the window
        BufferedImage minus = ImageIO.read(this.getClass().getResource("/images/minus.png"));
        JButton btnMinimizeMainwindow = createWindowControlButton(
                new ImageIcon(minus),
                (event) -> {
                    setExtendedState(getExtendedState() | ICONIFIED);
                });

        // maximize/un-maximize(??) the window
        BufferedImage plus = ImageIO.read(this.getClass().getResource("/images/plus.png"));
        JButton btnMaximizeMainwindow = createWindowControlButton(
                new ImageIcon(plus),
                (event) -> {
                    int currentExtendedState = getExtendedState();
                    if((currentExtendedState & MAXIMIZED_BOTH) != 0) {
                        setExtendedState(NORMAL);
                    } else {
                        setExtendedState(currentExtendedState | MAXIMIZED_BOTH);
                    }
                });

        background.setLayout(new FlowLayout());
        // order is important here if you care where which button goes
        background.add(btnMinimizeMainwindow);
        background.add(btnMaximizeMainwindow);
        background.add(btnCloseMainwindow);
    }

    private JButton createWindowControlButton(ImageIcon icon, ActionListener clickEventListener) {
        JButton btn = new JButton();
        btn.setIcon(icon);
        // transparent button
        btn.setOpaque(false);
        btn.setContentAreaFilled(false);
        btn.setBorderPainted(false);
        // click event
        btn.addActionListener(clickEventListener);

        return btn;
    }

    public static void main( String[] args ) throws IOException
    {
        final ImageIcon imageIcon = new ImageIcon(ImageIO.read(MainFrame.class.getResource("/splashscreen.png")));
        MainFrame.splash = new SplashWindow(imageIcon, "Loading images...");

        new MainFrame();
    }
}
