package org.darkwood.app.downeyjr;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

public class SplashWindow extends JFrame {
    private static final long serialVersionUID = 6841790118164279066L;
    private JProgressBar progressBar = new JProgressBar();
    private int imagesLoadedSoFar = 0;

    public SplashWindow(ImageIcon imageIcon, String message) {
        super();
        setUndecorated(true);
        JLabel imageLabel = new JLabel(imageIcon);
        imageLabel.setLayout(new BorderLayout());
        // horizontally align any text in the label to center
        final JLabel messageLabel = new JLabel(message, SwingConstants.CENTER);
        messageLabel.setForeground(Color.ORANGE);
        imageLabel.add(messageLabel, BorderLayout.SOUTH);
        setLayout(new BorderLayout());
        add(imageLabel, BorderLayout.CENTER);
        add(progressBar, BorderLayout.SOUTH);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        toFront();
    }
    
    public void setNumberOfImages(int numberOfImages) {
        this.imagesLoadedSoFar = 0;
        progressBar.setMinimum(0);
        progressBar.setMaximum(numberOfImages);
    }

    public void update(int imagesLoadedSoFar) {
        progressBar.setValue(imagesLoadedSoFar);
        this.imagesLoadedSoFar = imagesLoadedSoFar;
    }
    
    public void progressStep() {
        imagesLoadedSoFar++;
        progressBar.setValue(imagesLoadedSoFar);
    }

    public void close() {
        setVisible(false);
//        dispose();
    }
}
