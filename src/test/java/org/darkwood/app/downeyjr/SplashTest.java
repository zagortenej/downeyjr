package org.darkwood.app.downeyjr;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class SplashTest extends Frame implements ActionListener {
    private static final long serialVersionUID = -733066157979436057L;

    public SplashTest() throws IOException {
        super("SplashScreen demo");
        setSize(500, 300);
        setLayout(new BorderLayout());
        Menu m1 = new Menu("File");
        MenuItem mi1 = new MenuItem("Exit");
        m1.add(mi1);
        mi1.addActionListener(this);

        MenuBar mb = new MenuBar();
        setMenuBar(mb);
        mb.add(m1);
        final URL resourceStream = this.getClass().getResource("/splashscreen.png");
        final BufferedImage bufferedImage = ImageIO.read(resourceStream);
        final ImageIcon imageIcon = new ImageIcon(bufferedImage);
        final SplashWindow splash = new SplashWindow(imageIcon, "Loading images...");
        final int numberOfImages = 20;
        splash.setNumberOfImages(numberOfImages);

        for (int i = 0; i < numberOfImages; i++) {

//            splash.update(i);
            splash.progressStep();

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
        }

        splash.close();
        setVisible(true);
        toFront();
    }

    public void actionPerformed(ActionEvent ae) {
        System.exit(0);
    }

    public static void main(String args[]) throws IOException {
        new SplashTest();
    }
}
